// console.log("Hello!");

function login(username, password, role) {
	if (typeof username === 'undefined') {
		return "Inputs must not be empty.";
	} else if (username === "") {
		return "Inputs must not be empty.";
	} else if (typeof password === 'undefined') {
		return "Inputs must not be empty.";
	} else if (password === "") {
		return "Inputs must not be empty.";
	} else if (typeof role === 'undefined') {
		return "Inputs must not be empty.";
	} else if (role === "") {
		return "Inputs must not be empty.";
	} else {
		switch(role) {
		case 'admin':
			return("Welcome back to the class portal, admin!");
			break;
		case 'teacher':
			return("Thank you for logging in, teacher!");
			break;	
		case 'student':
			return("Welcome to the class portal, student!");
			break;
		default:
			return("Role out of range.");
			break;
		}
	}
}

message = login();
console.log(message);
message = login("", "password", "admin");
console.log(message);
message = login("adminUser", "", "admin");
console.log(message);
message = login("adminUser", "password", "");
console.log(message);

message = login("adminUser", "password", "admin");
console.log(message);
message = login("teacherUser", "password", "teacher");
console.log(message);
message = login("studentUser", "password", "student");
console.log(message);
message = login("studentUser", "password", "carpenter");
console.log(message);




function checkAverage(num1, num2, num3, num4) {
	let variable = Math.round((num1 + num2 + num3 + num4)/4)

	if(variable <= 74){
		return("Hello, student, your average is: " + variable + ". " + "The letter equivalent if F")
	} else if(variable >= 75 && variable <= 79){
		return("Hello, student, your average is: " + variable + ". " + "The letter equivalent if D")
	} else if(variable >= 80 && variable <= 84){
		return("Hello, student, your average is: " + variable + ". " + "The letter equivalent if C")
	} else if(variable >= 85 && variable <= 89){
		return("Hello, student, your average is: " + variable + ". " + "The letter equivalent if B")
	} else if(variable >= 90 && variable <= 95){
		return("Hello, student, your average is: " + variable + ". " + "The letter equivalent if A")
	} else if(variable >= 96){
		return("Hello, student, your average is: " + variable + ". " + "The letter equivalent if A+")
	}	
}

message = checkAverage(71,70,73,71);
console.log(message);
message = checkAverage(75,75,76,78);
console.log(message);
message = checkAverage(80,82,83,81);
console.log(message);
message = checkAverage(85,86,85,86);
console.log(message);
message = checkAverage(91,90,92,90);
console.log(message);
message = checkAverage(95,96,97,96);
console.log(message);


//Do not modify
//For exporting to test.js
try {
    module.exports = {
       login, checkAverage
    }
} catch(err) {

}
