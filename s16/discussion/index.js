/*console.log("HI!");*/

// [SECTION] Arithmetic Operators
let x = 81, y = 9;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y; // asterisk for multiplication
console.log("Result of product operator: " + product);

let quotient = x / y; // forward slash for division
console.log("Result of quotient operator: " + quotient);

let remainder = x % y; // percent for remainder
console.log("Result of module operator: " + remainder);


// [SECTION] Assignment Operator
// Basic Assignment Operator (=)
// The assignment operator assigns the value of the **right** operand to a variable.

let assignmentNumber = 8;

// Addition Assignment Operator
// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.

// assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2; // shorthand
console.log("Result of addition assignment operator: " + assignmentNumber);

/*
	Mini-Activity:
		1. Use assignment operators to other arithmetic operators.
		2. Console log the results.
		3. Send screenshots to our hangouts
*/

assignmentNumber -= 2; 
console.log("Result of subtraction assignment operator: " + assignmentNumber);

assignmentNumber *= 2; 
console.log("Result of multiplication assignment operator: " + assignmentNumber);

assignmentNumber /= 2; 
console.log("Result of division assignment operator: " + assignmentNumber);

assignmentNumber %= 2; 
console.log("Result of module assignment operator: " + assignmentNumber);


/*
	JS follows the PEMDAS rule:
	1. 3 * 4 = 12
	2. 12 / 5 = 2.4
	4. 1 + 2 = 3
	5. 3 - 2.4 = 0.6
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of mdas: " + pemdas);

// Increment and Decrement
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied

/*
1) Post-Increment (i++): we use i++ in our statement if we want to use the current value, and then we want to increment the value of i by 1.

2) Pre-Increment(++i): We use ++i in our statement if we want to increment the value of i by 1 and then use it in our statement.
*/

let z = 1;

let increment = ++z; // pre-increment (no need for reassignment)
console.log("Result of pre-increment:" + increment);
console.log("Result of pre-increment:" + z);

increment = z++; // post-increment
console.log("Result of post-increment:" + increment);
console.log("Result of post-increment:" + z);

let decrement = --z; // pre-increment (no need for reassignment)
console.log("Result of pre-increment:" + decrement);
console.log("Result of pre-increment:" + z);

decrement = z--; // post-increment
console.log("Result of post-increment:" + decrement);
console.log("Result of post-increment:" + z);

// [SECTION] Type Coercion
/*
- Type coercion is the automatic or implicit conversion of values from one data type to another
- This happens when operations are performed on different data types that would normally not be possible and yield irregular results
- Values are automatically converted from one data type to another in order to resolve operations
*/

let numA = '10';
let numB = 12;
let	coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16, numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);

// [SECTION] Comparison Operators

// Equality Operator (==)
/* 
    - Checks whether the operands are equal/have the same content
    - Attempts to CONVERT AND COMPARE operands of different data types
    - Returns a boolean value
*/

let juan = 'juan';
console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1'); // data type doesn't matter as long as the value is the same
console.log("juan" == "juan");
console.log(juan == "juan");


// Inequality Operator (!=)
/* 
    - Checks whether the operands are not equal/have different content
    - Attempts to CONVERT AND COMPARE operands of different data types
*/

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log("juan" != "juan");
console.log(juan != "juan");

// Strict Equality Operator (===)

/* 
    - Checks whether the operands are equal/have the same content
    - Also COMPARES the data types of 2 values
    - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
    - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
    - Some programming languages require the developers to explicitly define the data type stored in variables to
	- Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
    - Strict equality operators are better to use in most cases to ensure that data types provided are correct
*/

console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log("juan" === "juan");
console.log(juan === "juan");


// Strict Inequality Operator (!==)
/* 
    - Checks whether the operands are not equal/have the same content
    - Also COMPARES the data types of 2 values
*/

console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log("juan"!== "juan");
console.log(juan !== "juan");


// [SECTION] Relational Operator
//Some comparison operators check whether one value is greater or less than to the other value.
// Has a boolean value

let a = 50;
let b = 65;

// GT (>)
let isGreaterThan = a > b;
console.log(isGreaterThan);

// LT (<)
let isLessThan = a < b;
console.log(isLessThan);

// GTE (>=)
let isGTorEqual = a>= b;
console.log(isGTorEqual);

// LTE (<=)
let isLTorEqual = a<= b;
console.log(isLTorEqual);

console.log(a > '30'); // forced coercion to change the string to a number 
console.log(a > 'twenty');

let str = 'forty'
console.log(b >= str); // NaN - not a number -- but will show false


// [SECTION] Logical Operator
let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&& - double ampersand)
let areAllRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + areAllRequirementsMet);

// Logical OR Operator (|| - double pipe)
// Returns true if one of the operands is true (1 + 0 = 1)
let areSomeRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + areSomeRequirementsMet);

// Logical NOT Operator (! - exclamation point)
// Return the opposite value
let areSomeRequirementsNotMet = !isRegistered
console.log("Result of logical NOT operator: " + areSomeRequirementsNotMet)

areSomeRequirementsNotMet = !isLegalAge
console.log("Result of logical NOT operator: " + areSomeRequirementsNotMet)






