const express = require("express");
const router = express.Router();

const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Route for creating a course
router.post("/", auth.verify, (req, res) => {

	// const data = {
	// 	course: req.body,
	// 	isAdmin: auth.decode(req.headers.authorization).isAdmin
	// }

	// courseController.addCourse(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	};

});


// Routes for retrieving all the courses
router.get("/all", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		courseController.getAllCourses().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}
});

router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

// Retrieving a specific course
		/*
			Steps:
			1. Retrieve the course that matches the course ID provided from the URL
		*/

router.get("/:id", (req, res) => {
	courseController.getSpecificCourse(req.params.id).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
})
// Route for updating a course
// JWT verification is needed for this route to ensure that a user is logged in before updating a course
router.put("/:id", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		courseController.updateCourse(req.params.id, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}
});

router.patch("/:id/archive", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		courseController.archiveCourse(req.params.id, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}
});


// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;