// CRUD Operations
		/*
		    - CRUD operations are the heart of any backend application.
		    - Mastering the CRUD operations is essential for any developer.
		    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
		    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
		*/

// insert documents (create)
/*

Insert One Document
	db.collectionName.insertOne({
		"fieldA": "valueA",
		"fieldB": "valueB"
	});

Insert Many Documents
	db.collectionName.insertMany([
		{
			"fieldA": "valueA",
			"fieldB": "valueB"
		},
		{
			"fieldA": "valueA",
			"fieldB": "valueB"
		}
	]);

*/
db.user.insertOne({
	"firstName": "Erlin",
	"lastName": "del Rosario",
	"mobileNumber": "+639123456789",
	"email": "erlindelrosario@mail.com",
	"company": "Zuitt"
});

db.user.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "87654321",
			email: "stephenhawking@mail.com"
		},
		courses: ["Python", "React", "PHP", "CSS"],
		department: "none"

	},

	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "98765432",
			email: "neilarmstrong@mail.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "none"

	}

]);

// Finding documents (Read/Retrieve)
/*

Syntax:
	- db.collectionName.find(); - find all
	- db.collectionName.find({field: value}); - all documents that will match the criteria
	- db.collectionName.findOne({}); - find first document
	- db.collectionName.findOne({field: value}); - first document that will match the criteria
	


*/

db.user.find();
db.user.find({firstName: "Stephen"}).pretty(); // doesn't work it didn't change
	

//Mini Activity
	/*
		1. Make a new collection with the name "courses"
		2. Insert the following fields and values

			name: Javascript 101
			price: 5000
			description: Introduction to Javascript
			isActive: true

			name: HTML 101
			price: 2000
			description: Introduction to HTML
			isActive: true
	*/


db.courses.insertMany([
	{
		"name": "Javascript 101",
		"price": 5000,
		"description": "Introduction to Javascript",
		"isActive": true	
	},
	{
	 	"name": "HTML 101",
		"price": 2000,
		"description": "Introduction to HTML",
		"isActive": true
	}
]);

// Updating/Replacing/Modifying Documents (Update)

/*

	Syntax:
	Updating One Document
		db.collectionName.updateOne(
			{
				field: value
			},

			{
				$set: {
					fieldToBeUpdated: value
				}
			}
		);

		- update the first matching document in our collection

	Updating Multiple/Many Documents
		db.collectionName.updateMany(
			{
				criteria: value
			},

			{
				$set: {
					fieldToBeUpdated: value
				}
			}
		)
*/

db.user.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"mobileNumber": "+639123456789",
	"email": "test@mail.com",
	"company": "none"
});

db.user.updateOne(
	{	
		"firstName": "Test"
	},

	{
		$set: {
		"firstName": "Bill",
		"lastName": "Gates",
		"mobileNumber": "123456789",
		"email": "billgates@mail.com",
		"company": "Microsoft"
		"status": "active"
		}
	}
);

db.courses.updateOne(
	{
		"name": "Javascript 101"
	},

	{
		$set: {
		"isActive": false
		}
	}
);

db.courses.updateMany(
	{},

	{
		$set: {
			"enrollees": 10
		}
	}
);

// Remove field 

db.user.updateOne(
	{
		"firstName": "Bill"		
	},

	{
		$unset: {
			"status": "active"
		}
	}
);


/*
	Syntax:
		Deleting One Document:
			db.collectionName.deleteOne({"critera": "value"})

		Deleting Multiple Documents:
			db.collectionName.deleteMany({"criteria": "value"})


*/

db.user.deleteOne({
	"company": "Microsoft"
});

db.user.deleteMany({
	"company": "none"
});

db.user.deleteMany({});
