let http = require("http");
let port = 4000;

http.createServer(function(request, response) {
	
	if(request.url == "/items" && request.method == "GET"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Data retrieved from the database");
		
	} else if (request.url == "/items" && request.method == "POST"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Data to be sent to the database");
	}

}).listen(port);

console.log("Running at localhost:4000");