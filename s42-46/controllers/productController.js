const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");


// ============ PRODUCT CREATION ============ //
module.exports.createProduct = (reqBody) => {
	return Product.findOne({name:reqBody.name}).then(result => {

		// No key-value pair
		if (reqBody.name == null && reqBody.description == null && reqBody.price == null) { 
			return {
				status: "Product creation failed.",
				message: "Please provide the name, description, and price of the product you'd like to add."
			}
		} 

		// Missing key-value pair
		else if (reqBody.name == null || reqBody.description == null || reqBody.price == null) { 
			return {
				status: "Product creation failed.",
				message: "Please provide the name, description, and price of the product you'd like to add."
			}
		} 

		// No data provided
		else if (reqBody.name == "" && reqBody.description == "" && reqBody.price == "") { 
			return {
				status: "Product creation failed.",
				message: "Please provide the name, description, and price of the product you'd like to add."
			}
		} 
		
		// No product name provided
		else if (reqBody.name == "") { 
			return {
				status: "Product creation failed.",
				message: "Please provide the product name."
			}
		} 

		// No product description provided
		else if (reqBody.description == "") { 
			return {
				status: "Product creation failed.",
				message: "Please provide the product description."
			}
		} 

		// No product name provided
		else if (reqBody.price == "") { 
			return {
				status: "Product creation failed.",
				message: "Please provide the product price."
			}
		} 

		// Provided an exisiting active product 
		else if (result != null && result.name == reqBody.name && result.isActive) {
			return {
				status: "Product creation failed.",
				message: "Product already exists. If you wish to edit/modify its product details, you have the option to do so."
			}
		}

		// Provided an exisiting archived product 
		else if (result != null && result.name == reqBody.name && !result.isActive) {
			return {
				status: "Product creation failed.",
				message: "Product already exists but is currently archived. If you wish to activate it, you have the option to do so."
			}
		} 

		// Filled out all mandatory fields and provided a new product name.
		else {
			let newProduct = new Product ({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			});

			return newProduct.save().then(product => {

				// Successful creation of product
				if (product) {
					return {
						message: reqBody.name + " has been added to the product list.",
						product
					}
				}

				// Error
				else {
					return {
						 message: "Unsuccessful product creation."
					}
				}
			})
		}
	}).catch(err => err)
};





// ============ RETRIEVAL OF ALL PRODUCTS ============ //
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => result).catch(err => err);
};





// ============ RETRIEVAL OF ALL ACTIVE PRODUCTS ============ //
module.exports.getAllActiveProducts = () => {
	return Product.find({isActive:true}).then(result => result).catch(err => err);
};





// ============ RETRIEVAL OF ALL ARCHIVED PRODUCTS ============ //
module.exports.getAllArchivedProducts = () => {
	return Product.find({isActive:false}).then(result => result).catch(err => err);
};





// ============ RETRIEVAL OF SPECIFIC PRODUCTS ============ //
module.exports.getSpecificProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {

		// Archived Product
		if (!result.isActive) {
			return {
				message: "Product is no longer available.",
				result
			}
		}

		// Active Product
		else {
			return result
		}
	}).catch(err => err)
};





// ============ UPDATE PRODUCT INFORMATION ============ //
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	// "The product is already archived and cannot be further modified. Please choose an active product to edit."

	return Product.findByIdAndUpdate(reqParams, updatedProduct, {new:true}).then(updatedProduct => {
		if (updatedProduct) {
			return {
				status: "Product has been updated successfully.",
				updatedProduct
			}
		} else {
			return {
				status: "The update was not successful."
			}
		}
	}).catch(err => err);
};






// ============ ARCHIVAL OF PRODUCTS ============ //
module.exports.archiveProduct = (reqParams) => {
	let archivedProduct = {
		isActive: false
	};

	return Product.findByIdAndUpdate(reqParams, archivedProduct).then(product => {
		if (!product.isActive) {
			return {
				status: "Product was already archived."
			}
		}

		else {
			return {
				status: "Product has been archived and is now marked as inactive.",
				product
			}
		}
	}).catch(err => err)
};





// ============ ACTIVATION OF PRODUCTS ============ //
module.exports.activateProduct = (reqParams) => {
	let activatedProduct = {
		isActive: true
	};

	return Product.findByIdAndUpdate(reqParams, activatedProduct).then(product => {
		if (product.isActive) {
			return {
				status: "Product is currently active."
			}
		} 

		else {
			return {
				status: "Product has been restored and is now marked as active.",
				product
			}
		}
	}).catch(err => err)
};