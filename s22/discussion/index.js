console.log("Hi!");

// Array Methods
/*

	1. Mutator Methods
		- seeks to modify the contents of an array
		- mutator methods are functions that mutate or change an array after they are created. These methods manipulate the original array perfoming various tasks such as adding or removing elements

*/

let fruits = ["Apple", "Banana", "Orange", "Mango"]

/*

	push()
		- adds an element AT THE END of an array and it returns array's length

*/

console.log("Current Fruits Array");
console.log(fruits);

// Adding element/s
let fruitlength = fruits.push("Pineapple");
console.log(fruitlength);
console.log("Mutated Array using push() method");
console.log(fruits);

fruits.push("Avocado", "Jackfruit", "Guava", "Pomelo");
console.log(fruits);

/*

	pop()
		- removes the LAST ELEMENT in our array and returns the removed element (when value is passed in another variable).

*/

let removeFruit = fruits.pop();
console.log(removeFruit);
console.log("Mutated Array using pop() method");
console.log(fruits);
fruits.pop();
console.log(fruits);

/*

	unshift()
	- adds one or more elements AT THE BEGINNING of an array and returns the length of the array (when value is passed in another variable).

	Syntax:
		arrayName.unshift(element);
		arrayName.unshift(elementA, elementB, elementC...);

*/

let unshiftLength = fruits.unshift("Lemon");
console.log(unshiftLength);
console.log("Mutated Array using unshift() method");
console.log(fruits);

fruits.unshift("Kiwi", "Strawberry");
console.log(fruits);


/*

	shift()
		- removes an element AT THE BEGINNING of our array and returns the removed element when stored in a variable.

	Syntax:
		arrayName.shift(element);
		arrayName.shift(elementA, elementB, elementC...);

*/

let shiftFruit = fruits.shift();
console.log(shiftFruit);
console.log("Mutated Array using shift() method");
console.log(fruits);

/*

	splice()
		- allows to simultaneously remove elements from a specified index number and adds an element
	
	Syntax:
		arrayName.splice(StartingIndex, deleteCount, elementsToBeAdded);

		startingIndex -- where to start the changes
		deleteCount -- how many to remove

*/

let spliceFruits = fruits.splice(1, 2, "Cherry", "Dragon Fruit"); // kind of replaced index 1 and 2 with Cherry and Dragon Fruit
console.log(spliceFruits);
console.log("Mutated Array using splice() method");
console.log(fruits);

fruits.splice(5, 3);
console.log(fruits);

/*

	sort()
		- rearranges the array elements in alphanumeric order

		Syntax:
			arrayName.sort();

*/

fruits.sort();
console.log("Mutated Array using sort() method");
console.log(fruits);

let mixedArr = [50, 10, 1, 8, "Emvir", "Adrian", undefined, [], "Marvin", "Joseph", "Zebra", "@", "!", "#", "$", "a", "b", "c"];
console.log(mixedArr.sort());

/*

	reverse()
		- reverses the order of the element in an array

		Syntax:
			arrayName.reverse();

*/

fruits.reverse();
console.log("Mutated Array using reverse() method");
console.log(fruits);

// shorthand for descending order
// console.log(fruits.sort().reverse());


/*MINI ACTIVITY:
- Create a function which will allow us to list fruits in the fruits array.
	-- this function should be able to receive a string.
	-- determine if the input fruit name already exist in the fruits array.
		*** If it does, show an alert message: "Fruit already listed on our inventory".
		*** If not, add the new fruit into the fruits array ans show an alert message: "Fruit is not listed in our inventory."
	-- invoke and register a new fruit in the fruit array.
	log the updated fruits array in the console
*/

/*function registerFruit (fruitName) {
 		let doesFruitExist = fruits.includes(fruitName);

 		if(doesFruitExist) {
 			alert(fruitName + " is already on our inventory");
 		} else {
 			fruits.push(fruitName);
 			alert("Fruit is now listed in our inventory");
 		}
 	}

registerFruit("Cherry");*/



/*

	2. Non-Mutator Methods
		- these are methods/functions that do not modify or change an array they are created. These methods also do not manipulate the original array but still performs various tasks such as returnning elements from an array

*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DK", "PH"];
console.log(countries);	

/*

	indexOf()
		- returns the index number of the FIRST MATCHING element found in an array. If no match wss found, the result will be -1. The search process will be done from out first element proceeding to the last element.

		Syntax: 
			arrayName.indexOf(searchValue);
			arrayName.indexOf(searchValue, fromIndex);
*/

let firstIndex = countries.indexOf("PH");
console.log(firstIndex);
firstIndex = countries.indexOf("PH", 4);
console.log(firstIndex);
firstIndex = countries.indexOf("PH", 7);
console.log(firstIndex);
firstIndex = countries.indexOf("DE");
console.log(firstIndex);
firstIndex = countries.indexOf("PH", -1); // -1 is non-existing so it will return -1 still
console.log(firstIndex);



/*

	lastIndexOf()
		- returns the index number of the last matching element found in an array. The search process will be done from the last element proceeding to the first element.

		Syntax:
			arrayName.lastIndexOf(searchValue);
			arrayName.lastIndexOf(searchValue, startingFromIndex);

*/

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf(): " + lastIndex);
lastIndex = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf(): " + lastIndex);


/*

	slice()
		- portions/slices elements from our array and return a new array

		Syntax:
			arrayName.slice(startingIndex);
			arrayName.slice(startingIndex, endingIndex);

*/

console.log(countries);

// Slicing elements from specified index to the last element
let sliceArrayA = countries.slice(2);
console.log("Result of slice() method");
console.log(sliceArrayA);
console.log(countries);

// Slicing elements from a specified index to another index (endingIndex is not included to the slice)
let sliceArrayB = countries.slice(0, 4);
console.log("Result of slice() method");
console.log(sliceArrayB);

// Slicing elements starting from the last element of an array
let sliceArrayC = countries.slice(-3);
console.log("Result of slice() method");
console.log(sliceArrayC);

/*
	toString()
		-returns an array as a string separated by commas.
		-is used internally on JS when an object needs to be displayed as a text (like in HTMl), or when an object needs to be used as string

		Syntax:
			arrayName.toString();

*/

let stringArray = countries.toString();
console.log("Result of toString() method");
console.log(stringArray);

/*

	concat()
		- combines two or more arrays and returns the combined result

		Syntax:
		 arrayA.concat(arrayB);
		 arrayA.concat(elementA);

*/

let tasksA = ["driving", "climbing", "cooking"];
let tasksB = ["playing", "baking"];
let tasksC = ["coding", "programming"];

let tasks = tasksA.concat(tasksB);
console.log("Result of concat() method: ");
console.log(tasks);

// Combined multiple arrays
let allTasks = tasksA.concat(tasksB, tasksC);
console.log(allTasks);

// Combined arrays with elements (similar to push())
let combinedTasks = tasksA.concat("shopping", "sleeping");
console.log(combinedTasks);

/*

	join()
	- returns an array as a string.
	- does not change the original array.
	- any separator can be speciified. The default separator is comma (,).

	Syntax:
		arrayName.join("separatorSymbol");
*/

let students = ["James", "Jhun", "Jimbo"];
console.log(students);
console.log(students.join("|"));
console.log(students.join(" "));
console.log(students.join(" - "));


/*

	3. Iteration Methods
		- are loops designed to perform repetitive tasks on arrays. This is useful for manipulating array data resulting in complex tasks.
		- normally work with a function is supplied as an argument.
		- aims to evaluate each element in an array.

*/

/*

	forEach()
		- similar to for Lopp that iterates on each array element

		Syntax:
			arrayName.forEach(function(individualElements){
				statement/business logic
			})

*/


allTasks.forEach(function(task){
	console.log(task);
});

let filteredTask = [];

allTasks.forEach(function(task){
	console.log(task);

	// characters of the elements this is now the strings
	if(task.length > 10){
		// Add the element/item to the filteredTask Arrays
		filteredTask.push(task);
	}
});

// bali yung parameter sa forEach given na ng array unlike sa normal na function pwede ibigay ng user as argument

console.log("Result of forEach() method");
console.log(filteredTask);

console.log(allTasks);

/*

	map()
		- Iterates on each element AND returns new array with different values depending on the result of the function's operation
		- This is useful for performing tasks where mutating/changing the elements are required
		- Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation


		Syntax:
			let/const resultArray = arrayName.map(function(individualElement){
				return statement;
			})
*/


let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number * number;
});

console.log(numbers);
console.log("Result of map() method: ");
console.log(numberMap);

/*

	every()
		- checks if all elements in an array met the given condition
		- returns a "true" value if all elements meet the condition and "false" if otherwise.

		Syntax:
			let/const resultArray = arrayName.every(function(individualelement){
				return expression/condition
			});

*/

let allValid = numbers.every(function(number) {
	return (number < 6);
});

console.log("Result of every() method");
console.log(allValid); 

/*
	
	some()
		- checks if at least one element in the array meet the given condition. Returns a "true" value is at least one element meets the given condition and false if otherwise.

	Syntax:
		let/const resultArray = arrayName.some(function(individualelement){
			return expression/condition
		});

*/

let someValid = numbers.some(function(number){
	return (number <= 0);
});

console.log("Result of some() method");
console.log(someValid);

/*
	filter()
		- returns a new array that contains elements which meet the given condiiton. Returns an empty array if no elements were found that satisfy the given condition.

	Syntax:
		let/const resultArray = arrayName.filter(function(individualelement){
			return expression/condition
		});
*/

let filterValid = numbers.filter(function(number){ // number para for each element
	return (number < 3);
});

console.log("Result of filter() method");
console.log(filterValid);

let nothingFound = numbers.filter(function(number){ // number para for each element
	return (number == 0);
});

console.log("Result of filter() method");
console.log(nothingFound);


// Filtering using forEach()
let filterNumbers = [];
numbers.forEach(function(number){
	if(number < 4){
		filterNumbers.push(number)
	}
});

console.log("Result of filtering using forEach() method: ");
console.log(filterNumbers);
console.log(numbers);


/*

	includes()
		- checks if the argument passed can be found in the array.
		- methods can be "chained" using them one after another. The result of the first methid is being used on the second method until all the "chained" methods have been resolved.

*/

let products = ["mouse", "keyboard", "LAPTOP", "monitor"];

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a");
});

console.log(filteredProducts);