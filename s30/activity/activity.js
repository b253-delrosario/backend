// Use the count operator to count the total number of fruits on sale.

db.fruits.aggregate([
	{$match: {"onSale":true}},
	{$group: {"_id": "$origin", "fruitsOnSale": {$sum:"$stock"}}},
	{$count: "fruitsOnSale"}
]);


// Use the count operator to count the total number of fruits with stock more than 20.

db.fruits.aggregate([
	{$match: {"stock": {$gte:20}}},
	{$group: {"_id": "$origin", "enoughStock": {$sum:"$stock"}}},
	{$count: "enoughStock"}
]);


// Use the average operator to get the average price of fruits onSale per supplier.

db.fruits.aggregate([
	{$match: {"onSale":true}},
	{$group: {"_id": "$supplier_id", "avg_price": {$avg:"$price"}}}
]);


// Use the max operator to get the highest price of a fruit per supplier.

db.fruits.aggregate([
	{$match: {"onSale":true}},
	{$group: {"_id": "$supplier_id", "max_price": {$max:"$price"}}}
]);


// Use the min operator to get the lowest price of a fruit per supplier.

db.fruits.aggregate([
	{$match: {"supplier_id": {$in: [1,2]}}},
	{$group: {"_id": "$supplier_id", "min_price": {$min:"$price"}}},
	{$sort: {"min_price": -1}}
]);



