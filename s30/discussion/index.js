db.fruits.insertMany([
			{
				name : "Apple",
				color : "Red",
				stock : 20,
				price: 40,
				supplier_id : 1,
				onSale : true,
				origin: [ "Philippines", "US" ]
			},

			{
				name : "Banana",
				color : "Yellow",
				stock : 15,
				price: 20,
				supplier_id : 2,
				onSale : true,
				origin: [ "Philippines", "Ecuador" ]
			},

			{
				name : "Kiwi",
				color : "Green",
				stock : 25,
				price: 50,
				supplier_id : 1,
				onSale : true,
				origin: [ "US", "China" ]
			},

			{
				name : "Mango",
				color : "Yellow",
				stock : 10,
				price: 120,
				supplier_id : 2,
				onSale : false,
				origin: [ "Philippines", "India" ]
			}
		]);


// MongoDB Aggregation Method
// Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data

// Single-Purpose Aggregation Operations
// If we only need simple aggregations with only 1 stage
db.fruits.count();

/*
	- The "$match" is used to pass the documents that meet the specified conditions
	 Syntax: { $match: {"field": "value"}}
	- The "$group" is used to group elements together and field-value pairs using the data from the grouped elements
	Syntax: { $group: {"_id": "value", "fieldResult": "valueResult"}}
*/

//  Pipelines with multiple stages
db.fruits.aggregate([
	{ 
		$match: {
			"onSale": true
		}
	},
	{
		$group: {
				"_id": "$supplier_id", // grouped by supplier
				"total": {$sum: "$stock"} // total values of all "stocks" 
		}
	}
]);

// Field Projection with Aggregation
// $project can be used aggregating data to include/exclude field from the returned results

/*
syntax:

{ $project: { "field": 1/0}}
*/

db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {"_id": "$supplier_id", "total":{$sum: "$stock"}}},
	{$project: {"_id": 0}}
]);

// mini activity
/*
	aggeregate all fruits that has a stock greater than or equal 20
	group by supplier id 
	show the sum of all stocks
*/

db.fruits.aggregate([
	{$match: {"stock": {$gte: 20}}},
	{$group: {"_id": "$supplier_id", "total":{$sum: "$stock"}}},
]);

db.fruits.aggregate([
	{$match: {"onSale":true}},
	{$group: {"_id": "$supplier_id", "total":{$sum:"$stock"}}}
	{$sort: {"total": -1}}
])

// Aggregating results based on array fields

/*The "$unwind" deconstructs an array field from a collection/field with an array value to output a result for each element.*/

db.fruits.aggregate([
	{$unwind: "$origin"}
]);

db.fruits.aggregate([
	{$unwind: "$origin"},
	{$group: {"_id": "$origin", "kinds": {$sum:1}}}
]);

//===== Schema Design ======//

let owner = ObjectId();

db.owners.insertOne({
	”_id”: owner,
	”name”:  “John Smith”,
	”contact”: "09123456789"
});

// Reference Data Model
db.suppliers.insertOne({
	”name”:  “ABC fruits”,
	”contact”: "09987654321"
	”owner_id”: <owner_id>
});

db.suppliers.insertOne({
	”name”:  “DEF fruits”,
	”contact”: "09321654987",
	”address”: [
		{
			”street”: “123 San Jose St.”,
			”city”: “Manila”
		},
		{
			”street”: “367 Gil Puyat”,
			”city”: “Makati”
		}
	]
});