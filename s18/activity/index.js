// console.log("Hi");

function addNum(addend1, addend2) {
	console.log("Displayed sum of " + addend1 + " and " + addend2);
	console.log(addend1 + addend2);
};

addNum(5, 15);




function subNum(minuend, subtrahend) {
	console.log("Displayed difference of " + minuend + " and " + subtrahend);
	console.log(minuend - subtrahend);
};

subNum(20, 5);




function multiplyNum(factor1, factor2) {
	let multiplyFormula = factor1 * factor2;
	console.log("The product of " + factor1 + " and " + factor2 + ":");
	return multiplyFormula;
};

let product = multiplyNum(50,10);
console.log(product);




function divideNum(dividend, divisor) {
	let divideFormula = dividend / divisor;
	console.log("The quotient of " + dividend + " and " + divisor + ":");
	return divideFormula;
};

let quotient = divideNum(50, 10);
console.log(quotient);




function getCircleArea(radius) {
	let circleAreaFormula = 3.1416 * (radius ** 2);
	console.log("The result of getting the area of a circle with " + radius + " radius: ");
	return circleAreaFormula;
};

let circleArea = getCircleArea(15);
console.log(circleArea);




function getAverage(num7, num8, num9, num10) {
	let averageFormula = (num7 + num8 + num9 + num10)/4
	console.log("The average of " + num7 + " " + num8 + " " + num9 + " and " + num10 + ":");
	return averageFormula;
};

let averageVar = getAverage(20, 40, 60, 80);
console.log(averageVar);




function checkIfPassed(score, total) {
	let percentage = (score/total) * 100;
	let isPassed = percentage > 75;
	console.log("Is " + score + "/" + total + " a passing score?");
	return isPassed;
};

let isPassingScore = checkIfPassed(38,50);
console.log(isPassingScore);



//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum, 
		subNum, 
		multiplyNum, 
		divideNum, 
		getCircleArea, 
		getAverage, 
		checkIfPassed
	}
} catch (err) {

}